import boto3

client = boto3.client('ec2')

def lambda_handler():
    response = client.authorize_security_group_ingress( 
        GroupId='sg-0aba990a9a79f340d',
        IpPermissions=[
            {
                'FromPort': 22,
                'IpProtocol': 'tcp',
                'IpRanges': [
                    {
                        'CidrIp': '1.1.1.2/32',
                        'Description': 'Test IP',
                    },
                ],
                'ToPort': 22,
            }
        ],
    )
    print(response)

lambda_handler()










